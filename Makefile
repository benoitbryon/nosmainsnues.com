.PHONY: develop html css js img public clean distclean maintainer-clean

MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT_DIR := $(patsubst %/,%,$(dir $(MAKEFILE_PATH)))
PROJECT_NAME := $(notdir $(PROJECT_DIR))
BUILD_DIR := $(PROJECT_DIR)/public

YARN ?= yarn
PARCEL ?= $(YARN) run parcel
PIPENV ?= pipenv


#: develop - Install development libraries.
develop:
	$(YARN) install
	$(PIPENV) install


# html - Generate HTML from content and templates.
html:
	$(PIPENV) run pelican -o public


img:
	mkdir -p public
	cp -r assets/img public


css:
	$(PARCEL) build --no-content-hash --dist-dir=public assets/css/main.scss


js:
	$(PARCEL) build --no-content-hash --dist-dir=public assets/js/main.js


#: public - Generate public/ folder contents from HTML and theme.
public: html css js img


#: serve - Serve public/ folder.
serve:
	cd public && $(PIPENV) run python3 -m http.server


#: clean - Basic cleanup, mostly temporary files.
clean:


#: distclean - Remove local builds
dist-clean: clean
	rm -rf html/
	rm -rf public/


#: maintainer-clean - Remove almost everything that can be re-generated.
maintainer-clean: dist-clean
	rm -rf .parcel-cache/
	rm -rf .pnp.*
	rm -rf .yarn*
