# LICENSE

The content of the website nosmainsnues.com by Benoît Bryon is licensed under CC BY-SA 4.0,
except for resources that are explicitely licensed under other terms.

See https://creativecommons.org/licenses/by-sa/4.0/ for details.
