# NosMainsNues.com

This is the code repository for nosmainsnues.com website,
including both content and theme.

Typical development/build environment requires:

* make. One setup on Debian is:
  * sudo apt-get install build-essential
* Python and pipenv. One setup on Debian is:
  * python3 -m pip install --user pipx
  * pipx install pipenv
* Yarn from NodeJS
* Git

Typical workflow:

* Clone the repository: git clone git@github.com:benoitbryon/nosmainsnues.git
* Go to repository folder: cd nosmainsnues
* Install build tools: make develop
* Generate public files: make public
* Run local HTTP server to check generated files: make serve
* Open generated files: firefox http://localhost:8000/
