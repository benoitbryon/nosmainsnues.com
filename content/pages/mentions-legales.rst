################
Mentions légales
################

:date: 2018-02-19
:modified: 2024-06-25
:slug: mentions-legales


***************
Éditeur du site
***************

.. line-block::

   **Benoît BRYON**
   4 B rue Jéliotte 64400 Oloron-Sainte-Marie, France
   +33.6.74.49.68.81
   benoit@marmelune.net

   **Statut :** auto-entrepreneur à Oloron-Sainte-Marie
   **SIRET :** 515 153 807 00034
   **NAF :** 7022Z
   TVA non applicable, article 293B du Code général des impôts (CGI)

*********
Hébergeur
*********

.. line-block::

   **OVH**
   2 rue Kellermann 59100 Roubaix, France
   +33.9.72.10.10.07


*******
Licence
*******

Sauf mention spécifique au cas par cas, les contenus publiés sur le site
*nosmainsnues.com* sont l'oeuvre de Benoît BRYON et sont publiés sous licence
`Creative Commons BY-NC-SA <https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr>`_.
