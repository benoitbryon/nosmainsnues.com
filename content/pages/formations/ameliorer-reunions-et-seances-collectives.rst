#########################################
Améliorer réunions et séances collectives
#########################################

:date: 2022-04-01
:modified: 2022-04-01 12:00
:slug: formations/ameliorer-reunions-et-seances-collectives
:tags: formation, réunion, intelligence collective, synkrominos
:category: formations
:subtitle: Atelier, de 3h30 à 1 jour
:summary: Découvrez des outils accessibles et puissants pour faciliter les
          réunions et autres séances de travail collectif ;
          partagez problématiques, expériences et créativité entre pairs ;
          bénéficiez des éclairages d'un facilitateur expérimenté ;
          et choisissez les prochains défis pour vos cas concrets ! 
:illustration: /img/2020-09-04-synkrominos-900x600.jpg


.. role:: raw-html(raw)
   :format: html

.. figure:: /img/reunions/atelier04-600x400px.jpg
   :alt: Les étapes d'une réunion représentées par des cartes, avec des personnages de jeu et des sabliers


**Vous contribuez à de beaux projets et vous souhaitez que les temps collectifs y soient à la fois agréables et porteurs de sens ?**

**Comme beaucoup, vous avez vécu des réunions très longues, laborieuses, tendues, à sens unique, etc.,
et vous cherchez des recettes pour vivre des moments plus fluides et constructifs ?**

**Venez découvrir des outils pratiques de l'intelligence collective, partager vos expériences, et préparer vos prochaines réunions !**

Atelier-formation en deux parties :

* `Améliorer nos réunions <#partie1>`_, base : 3h30
* `Les coulisses des réunions <#partie2>`_, approfondissement : 3h30

:raw-html:`<p style="text-align:center;"><a href="#dates-et-inscriptions" class="btn btn-primary">Je m'inscris :)</a></p>`


*********
Programme
*********

.. figure:: /img/reunions/atelier06-600x400px.jpg
   :alt: Les étapes d'une formation représentées par des cartes

Matin : atelier « pour des réunions efficaces, sereines et joyeuses »
=====================================================================

3h30 d'atelier pour l'amélioration des réunions :

* ouverture pour synchroniser du groupe ;
* analyse de pratiques ;
* partage d'expériences et de bonnes pratiques ;
* créativité et plan d'action ;
* clôture.

`Détails de la partie 1 <#partie1>`_ ci-après.

Après-midi : les coulisses des réunions
=======================================

3h30 pour élargir et/ou approfondir le sujet des réunions :

* identification des sujets ;
* choix et traitement des sujets ;
* ancrage et plan d'action.

`Détails de la partie 2 <#partie2>`_ ci-après.


**********************
Informations pratiques
**********************

* **Niveau :** tous niveaux. débutants et expérimentés bienvenus :)
* **Prérequis :** aucun !
* **Public :** Tous publics francophones
* **Nombre de participants :** 6 minimum, 10 maximum
* **Durée et horaires :** 2 demi-journées possiblement consécutives, matin 9h-13h30, après-midi 13h30-17h
* **Repas le midi :** repas non inclus dans la prestation, possibilité de déjeuner sur place, amenez votre casse-croûte :)


******
Tarifs
******

.. admonition:: Compte personnel formation & Formation professionnelle
   :class: note

   J'ai entamé des démarches pour que cette formation soit hébergée par un organisme
   de formation et puisse à ce titre être prise en charge dans le cadre du compte
   personnel formation (CPF) ou de la formation professionnelle continue (via les OPCO).
   Si vous êtes intéressés, `contactez-moi </#contact>`_.

Les tarifs suivants sont destinés aux participants finançant eux-mêmes la formation,
c'est à dire hors cadre de formation professionnelle continue (via OPCO) et Compte
personnel formation (CPF).

* **solo :** 150€ HT pour la participation d'une personne à la journée complète, ou 80€ HT pour la matinée seulement.
* **trio :** 360€ HT pour la participation de 3 personnes à la journée complète, ou 180€ HT pour la matinée seulement.
* **solidaire :** tarif libre à partir de 30 € HT (offre en quantité limitée).


*********************
Dates et inscriptions
*********************

Voici les prochaines sessions programmées ou en cours de mise en place...

Des sessions sont planifiées à l'avance pour que chacun puisse anticiper.
Les inscriptions seront confirmées si un seuil minimum de participants minimum est atteint,
sinon elles seront annulées à l'initiative du vendeur et les éventuels paiements remboursés.

Si vous souhaitez une intervention dans votre organisation ou près de chez vous,
`contactez-moi </#contact>`_ :)

.. raw:: html

  <a title="Logiciel billetterie en ligne" href="https://www.weezevent.com/widget_multi.php?238481.1.1.bo" class="weezevent-widget-integration" target="_blank" data-src="https://www.weezevent.com/widget_multi.php?238481.1.1.bo" data-width="100%" data-height="100%" data-id="multi" data-resize="1" data-npb="0" data-width_auto="1">Billetterie Weezevent</a><script type="text/javascript" src="https://www.weezevent.com/js/widget/min/widget.min.js"></script>


***********************
Intervenants et contact
***********************

La formation est proposée par Benoît Bryon.

Benoît BRYON
============

Facilitateur de dynamiques collectives, développeur de Communs.

  Ce qui m'anime, c'est d'identifier et de rendre accessibles les chemins qui nous
  permettent de mettre en cohérence nos voeux et nos actions. Dans cette démarche, je
  porte attention aux singularités de chaque personne et cherche à mettre en oeuvre le
  cadre propice à l'action collective.
  Ma propre singularité trouve un écho fort dans la documentation et la transmission
  d'histoires et de pratiques, sous forme de témoignages graphiques ou de supports-outils
  de facilitation (affiches, jeux, etc.).
  Je contribue à plusieurs projets collectifs tels que
  `l'élabomobile <https://elabomobile.org>`_ ou
  `Qu'est-ce que tu veux faire ? <http://questcequetuveuxfaire.org>`_
  au sein desquels j'exerce et affine les principes que je transmets.

https://nosmainsnues.com – 06 74 49 68 81 – benoit@marmelune.net


.. _partie1:

*********************************
Partie 1 : améliorer nos réunions
*********************************

.. figure:: /img/reunions/faire-comme-si-1080x1080px.jpg
   :alt: Dans les réunions, j'aime quand chacun exprime le meilleur de lui-même !

La première partie de cet atelier est littéralement une « réunion pour améliorer nos réunions » :

* faites l'expérience d'un processus d'intelligence collective animé par un facilitateur expérimenté ;
* découvrez des outils par la pratique, dont Synkrominos ;
* analysez vos pratiques, bénéficiez du partage d'expérience et de l'émulation créative entre pairs ;
* cherchez des pistes d'actions concrètes pour vos propres situations.

Apprendre par la pratique
=========================

Cette séquence est une expérience pratique : l'intervenant déroule un processus d'intelligence collective avec ses outils de prédilection, et c'est, en soi, un acte de transmission.

La posture de facilitateur, plutôt qu'animateur ou professeur, favorise l'écoute et les partages entre participants.
Les supports visuels rendent le processus à la fois pédagogique et accessible.
L'atmosphère se veut légère et créative avec un sujet bien sérieux : comment améliorer nos réunions ?

De l'intelligence collective, en pair à pair
============================================

Le processus invite les participants à identifier leurs propres problématiques et résolutions, de sorte qu'émergent les questions et les réponses les mieux adaptées à chacun, et de là les engagements les plus justes.

Les participants croisent leurs expériences et leurs outils pour mieux analyser leurs pratiques respectives et trouver des solutions adaptées à chacun.

.. figure:: /img/reunions/prolonger-1080x1080px.jpg
   :alt: Dans les réunions, j'aime finir à l'heure, satisfait et plein d'élan à agir !

Un temps de travail utile à court terme
=======================================

Cette séance est conçue pour être “productive”, c'est à dire utile assez directement à ses participants. Le processus invite à formuler pour soi-même des défis en vue d'améliorer une situation vécue ou de répondre à un besoin éprouvé. L'invitation porte sur des “petits pas” : des réalisations concrètes, accessibles et significatives. Chacun est bien sûr libre de son niveau d'engagement.

De fait, cet atelier peut être vécu plusieurs fois, comme un travail d'amélioration continue ! À chaque fois, faites le point sur votre expérience, ciblez une situation sur laquelle vous souhaitez agir, et formulez de nouveaux défis ! Si la première participation est une découverte complète, renouveler l'expérience permet de continuer la progression, sur son chemin collectif ou individuel. Il peut ainsi être utilisé pour soutenir l'organisation interne d'une entreprise ou d'un collectif.

Que vous soyez fraîchement engagés dans l'organisation de réunions ou bien sociocrates avertis, soyez les bienvenus ! D'une part la diversité des parcours est précieuse, et d'autre part, à partir d'une même histoire chacun trouvera les apprentissages à sa mesure, selon sa situation et ses besoins !

:raw-html:`<p style="text-align:center;"><a href="#dates-et-inscriptions" class="btn btn-primary">Je m'inscris :)</a></p>`



.. _partie2:

*************************************************
Partie 2 : approfondir connaissances et pratiques
*************************************************

.. figure:: /img/reunions/formation-1080x1080px.jpg
   :alt: Découvrez Synkrominos et d'autres outils lors d'une formation !

La deuxième partie de la formation est un temps d'analyse et d'approfondissement, où nous nous attachons aux coulisses d'une réunion : que nous apprend l'atelier de la première demi-journée ? Qu'est-ce qui se joue dans les situations auxquelles nous sommes confrontés ? Sur quoi agir pour contribuer à des changements positifs ?

L'intention est de choisir des sujets auxquels porter une attention particulière, en observer les intrigues et les enjeux, pour adapter nos stratégies. Les sujets pourront varier selon plusieurs paramètres tels l'expérience de la 1ère demi-journée ou les questions particulières des participants.

À titre d'exemple, voici quelques sujets qui ont pu être abordés lors de précédentes sessions :

* Comment utiliser Synkrominos dans mon contexte ? Quand et comment utiliser certaines cartes ?
* Coordination, stratégie, décision... pourquoi et pour quoi identifier différents types de réunions ?
* De quoi est fait le “cadre commun” dans une réunion ? Comment faire pour que les participants le respectent ?
* Quelle différence entre “animer” et “faciliter” une réunion ?
* Comment faire avec les retards et les absences ?
* Vote, consensus, consentement, sollicitation d'avis, stigmergie... Comment décider ensemble ?

Dans le temps imparti, seul un nombre limité de sujets pourra être traité, et cet arbitrage est justement l'occasion d'un exercice de prise de décision collective :)

Également, dans le cas de sujets très denses, l'intention sera davantage d'ouvrir des portes et d'avoir quelques clés pour y revenir par la suite. Par exemple, des sujets comme la prise de décision collective ou la gestion des tensions pourront être abordés, avec la conscience que leur exploration profonde mérite davantage de temps !

Enfin, le travail en petits groupes pourra être envisagé si cela permet de mieux répondre aux besoins de chacun.

:raw-html:`<p style="text-align:center;"><a href="#dates-et-inscriptions" class="btn btn-primary">Je m'inscris :)</a></p>`
