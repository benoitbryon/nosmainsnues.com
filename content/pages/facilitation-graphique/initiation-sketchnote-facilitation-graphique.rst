################################################
Initiation sketchnotes et facilitation graphique
################################################

:date: 2019-06-11
:modified: 2024-11-29 15:00
:slug: facilitation-graphique/initiation-sketchnote-facilitation-graphique
:tags: formation, atelier, sketchnotes, facilitation graphique, pensée visuelle, scribing
:category: formations
:subtitle: Ateliers et formations
:summary: Découvrez les bases de la prise de notes visuelles (*sketchnoting*) et de la
          facilitation graphique pour conjuguer plaisir et efficience dans vos prises de
          notes, pour vous exprimer, au service d'un travail collectif ou pour votre usage
          personnel.
:illustration: /img/facilitation-graphique/formation-420x280.jpg

.. role:: raw-html(raw)
   :format: html

.. figure:: /img/facilitation-graphique/2018-10-22-picto-facilitation-graphique-900x600.png
   :alt: En pictogrammes : des idées montant un escalier

   :raw-html:`<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="CC-BY-SA 4.0" src="/img/cc-by-sa.png" style="height:18px;" /></a>` Benoît Bryon

**Dans cette formation, expérimentez la prise de notes visuelles, la facilitation graphique,
et découvrez comment les appliquer dans votre contexte professionnel, associatif ou personnel.**

.. raw:: html

   <p style="text-align:center;">
     <a class="btn btn-outline-secondary" href="/img/facilitation-graphique/affiche-initiation-sketchnotes-et-facilitation-graphique.pdf">
       <img style="width:105px;" src="/img/facilitation-graphique/affiche-initiation-sketchnotes-et-facilitation-graphique-210x297px.jpg" /><br />
       Affiche
     </a>
     <a href="#inscription" class="btn btn-outline-primary">
       <span class="fa fa-calendar-check"> </span><br />
       Dates et inscriptions
     </a>
   </p>`

**La prise de notes visuelles** (*ou "sketchnoting" ou "croquinotes"*) permet de transcrire
des idées sur papier à l'aide de textes et de formes simples (*points, lignes, formes géométriques basiques*).
Si cette forme graphique peut être suffisamment légère pour prendre des notes en direct,
elle invite aussi à prendre du recul sur notre façon d'observer et de raconter le monde.

**La facilitation graphique** met à profit des éléments visuels pour accompagner un processus de travail.
Les supports peuvent être variés : sketchnotes en petit ou grand format, affiches, exercices avec des post-its, ...
Si la facilitation graphique est particulièrement adaptée pour soutenir l'intelligence collective,
elle questionne aussi la façon dont les éléments visuels peuvent aider chacun de nous à s'exprimer.

Cette formation vous permet de commencer avec les sketchnotes et la facilitation graphique,
concrètement, sur place, puis vous donne des clés pour continuer cette pratique
de façon adaptée à votre environnement de travail et à votre approche personnelle.


**********************
Objectifs pédagogiques
**********************

.. figure:: /img/facilitation-graphique/bandeau-atelier-900x300.jpg
   :alt: Photos d'une formation en facilitation graphique

Cette formation a pour but de vous mettre en capacité de...

* Différencier des pratiques comme le sketchnoting, le scribing ou la facilitation graphique ;
* Transcrire des idées visuellement avec la technique sketchnotes ;
* Prendre des notes visuelles en direct ;
* Utiliser la facilitation graphique pour soutenir le travail d’un groupe ;
* Composer un « kit visuel » : matériel, méthodologie, dictionnaire de pictogrammes, etc. ;
* Préparer la poursuite de la pratique dans un contexte professionnel après la formation.


*********
Programme
*********

.. admonition:: Versions courtes
  :class: tip

  Le programme indiqué ici se déroule sur 2 jours et offre ainsi un temps
  d'immersion dans le sujet. Il est possible de réaliser des sessions plus courtes
  en adaptant les objectifs. Par exemple, des ateliers de 2 heures ont déjà été
  réalisés auprès de publics scolaires. Pour plus d'informations,
  `contactez-moi </#contact>`_ :)

Jour 1
======

* **Mise en mouvement :** se (re)connecter au plaisir de dessiner,
  dépasser l’appréhension et l’auto-jugement ;

* **Technique sketchnotes :** découverte, feutre en main, du *vocabulaire graphique*.
  Explorations à la recherche de son propre style ;

* **Pratique sketchnotes :** utilisation de la technique des sketchnotes sur des
  cas concrets, dont une partie en *« prise de notes en direct »*. Regards croisés
  sur la singularité des pratiques.

Jour 2
======

* **Facilitation graphique :** exploration des outils visuels comme moyen
  d'expression de soi et de contribution à un travail collectif ;

* **Mise en application :** exercices pratiques adaptés aux besoins et projets
  des stagiaires ;

* **Plan d'action :** identification des actions concrètes et adaptées à son contexte
  qui permettent de continuer la pratique après la formation.


**********************
Informations pratiques
**********************

.. figure:: /img/facilitation-graphique/quel-est-votre-style-900x450.jpg
   :alt: Découverte du vocabulaire graphique

   :raw-html:`<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="CC-BY-SA 4.0" src="/img/cc-by-sa.png" style="height:18px;" /></a>` Benoît Bryon

* **Niveau :** débutant, atelier d’initiation
* **Pré-requis :** aucun. Pas besoin de *« savoir dessiner »* ! Si quelques
  traits basiques améliorent la lisibilité, la prise de notes visuelle
  fonctionne aussi avec seulement du texte !

* **Public :** Tous publics francophones
* **Modalité :** Présentiel
* **Nombre de participants :** 6 minimum, 12 maximum
* **Durée et horaires :** 14h réparties sur 2 jours consécutifs, horaire type 9h-17h
* **Repas le midi :** repas du midi inclus, prévu sur place ou à proximité immédiate


*********************
Tarifs et financement
*********************

.. class:: lead

   **!!! OFFRE SPÉCIALE EN COURS !!!**

   Tarification spéciale *"Aux rencontres des Vallées"* pour une formation à Lurbe-St-Christau (64) les 30 et 31 janvier 2025 :
   Formule *Standard* à 200 € HT :sup:`[1]`. Formule *Coup de pouce* disponible.
   `Inscriptions ci-dessous <#inscription>`_.

.. admonition:: Pas de certification QUALIOPI
  :class: note

  À ce jour, cette formation est une prestation de services proposée
  en dehors du cadre de la formation professionnelle continue.
  Sans certification QUALIOPI, cette formation n'est pas éligible aux financements
  via le Compte Personnel Formation (CPF) ou via les OPérateurs de COmpétences (OPCO).

Inter-entreprises
=================

Inscriptions individuelles aux sessions programmées par nos soins.
Pour deux jours de formation :

* 500 € HT :sup:`[1]` pour une personne (offre SOLO)
* 900 € HT :sup:`[1]` pour deux personnes (offre DUO, soit 10% d'économie par rapport à l'offre SOLO)
* 1200 € HT :sup:`[1]` pour trois personnes (offre TRIO, soit 10% d'économie par rapport à l'offre SOLO)

:sup:`[1]` *TVA non applicable, article 293 B du Code Général des Impôts*

.. figure:: /img/facilitation-graphique/formation-vocabulaire-900x300.jpg
   :alt: Découverte du vocabulaire graphique

Intra-entreprise
================

Inscription d'un groupe pour une date et un lieu de session à personnaliser.
Tarif pour un groupe de 4 à 12 personnes pour 2 jours de formation.

* sur devis.


.. _inscription:

*********************
Dates et inscriptions
*********************

Voici les prochaines sessions programmées :

.. raw:: html

   <a title="Logiciel billetterie en ligne" href="https://www.weezevent.com/widget_multi.php?238481.2.1.bo" class="weezevent-widget-integration" target="_blank" data-src="https://www.weezevent.com/widget_multi.php?238481.2.1.bo" data-width="100%" data-height="100%" data-id="multi" data-resize="1" data-npb="0" data-width_auto="1">Billetterie Weezevent</a><script type="text/javascript" src="https://www.weezevent.com/js/widget/min/widget.min.js"></script>


Si aucune des sessions proposées ici ne vous convient, vous pouvez
`solliciter l'ouverture de nouvelles dates et lieux via ce formulaire <https://framaforms.org/initiation-sketchnotes-et-facilitation-graphique-ca-minteresse-1677850825>`_.

Les sessions *proposées* seront *confirmées* s'il y a au moins 6 inscrits deux semaines avant la session,
sinon elles seront ajournées (*la réservation de salle est annulée*). Les personnes déjà inscrites
seront invitées à participer à d'autres sessions.

Si une session est *confirmée* et qu'il reste des places, il reste possible de s'inscrire
jusqu'à 1 semaine avant la session.

Une intervention dans votre organisation (intra-entreprise) ou près de chez vous est possible !
Prenez contact avec `le formateur`_ pour présenter votre projet :)


***********************
Intervenants et contact
***********************

* **Benoît Bryon** intervient en tant que formateur, responsable du contenu de la formation ;
* **une seconde personne**, avec un profil de facilitation en intelligence collective,
  est présente lors de la formation pour fluidifier le déroulé et aménager un cadre propice
  à l'apprentissage ;

Benoît BRYON
============

Facilitateur graphique & développeur de pratiques collectives.

  Sketchnotes et facilitation graphique sont des outils que j'utilise pour révéler, partager et
  mettre en oeuvre les idées. De manière plus large, j'aide des groupes à formuler et à explorer
  des façons où s'organiser et réaliser collectivement permet de surpasser les capacités et
  défis de chacun.

https://nosmainsnues.com – +33 6 74 49 68 81 – contact@nosmainsnues.com


.. _`le formateur`: /#contact
